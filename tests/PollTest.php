<?php

namespace App\Tests;

use App\Kernel;
use App\Service\PollBuilder;
use App\Entity\User;
use App\Entity\Poll;
use App\Entity\Option;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class PollTest extends TestCase
{
    public function testHomePageStatus()
    {
        $client = static::createClient();

        $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testHomePageHeader()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertSelectorTextContains('html h3.title', 'Aktywne ankiety');
    }

    public function testPollBuilder()
    {
        $builder = new PollBuilder();

        $poll = $builder->buildPoll(new User('test'), ['name' => 'x', 'text' => 'y']);

        $this->assertEquals(true, $poll->getIsRunning());
        $this->assertEquals('x', $poll->getName());
        $this->assertEquals('y', $poll->getText());
    }

    public function testPollOptionBuilder()
    {
        $builder = new PollBuilder();

        $pollOption = $builder->buildPollOption(new Poll(), 'x');

        $this->assertEquals('x', $pollOption->getText());
    }

    public function testDatabaseConnection()
    {
        $kernel = new Kernel;
        $kernel->boot();
        $container = $kernel->getContainer();

        $data = $container->get(EntityManagerInterface::class)->getRepository(PollVote::class)->findAll();
        
        $this->assertTrue(count($data) >= 0);
    }
}