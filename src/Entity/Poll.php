<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PollRepository")
 * @ORM\Table(name="polls")
 */
class Poll
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="polls_created")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    private $author;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_running;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $text;

    /**
     * @ORM\OneToMany(targetEntity="PollOption", mappedBy="poll")
     */
    private $options;

    /**
     * @ORM\OneToMany(targetEntity="PollVote", mappedBy="poll")
     */
    private $votes;

    public function __construct() {
        $this->options = new ArrayCollection();
        $this->votes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function setAuthor(User $user): self
    {
        $this->author = $user;

        return $this;
    }

    public function getIsRunning(): ?bool
    {
        return $this->is_running;
    }

    public function setIsRunning(bool $is_running): self
    {
        $this->is_running = $is_running;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function hasVoted($userId)
    {
        foreach ($this->votes as $vote)
        {
            if ($vote->getVoter()->getId() == $userId)
                return true;
        }

        return false;
    }

    public function getTotalVotes()
    {
        return count($this->votes);
    }
}
