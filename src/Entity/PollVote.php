<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PollVoteRepository")
 * @ORM\Table(name="poll_votes")
 */
class PollVote
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="votes")
     * @ORM\JoinColumn(name="voter_id", referencedColumnName="id")
     */
    private $voter;

    /**
     * @ORM\ManyToOne(targetEntity="Poll", inversedBy="votes")
     * @ORM\JoinColumn(name="poll_id", referencedColumnName="id")
     */
    private $poll;

    /**
     * @ORM\ManyToOne(targetEntity="PollOption", inversedBy="votes")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    private $option;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVoter(): User
    {
        return $this->voter;
    }

    public function setVoter(User $voter): self
    {
        $this->voter = $voter;

        return $this;
    }

    public function getPoll(): Poll
    {
        return $this->poll;
    }

    public function setPoll(Poll $poll): self
    {
        $this->poll = $poll;

        return $this;
    }

    public function getOption(): PollOption
    {
        return $this->option;
    }

    public function setOption(PollOption $option): self
    {
        $this->option = $option;

        return $this;
    }
}
