<?php
namespace App\Entity;
 
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;

 
/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="users") 
 */
class User implements UserInterface  {
 
   /**
    * @ORM\Column(type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   private $id;
 
   /**
    * @ORM\Column(type="string", length=128)
    */
   private $username;
 
   /**
    * @ORM\Column(type="string", length=4096)
    */
   private $password;

   /**
    * @ORM\Column(type="string", length=4096)
    */
    private $roles;
 
    /**
     * @ORM\OneToMany(targetEntity="PollVote", mappedBy="voter")
     */
    private $votes;

    /**
     * @ORM\OneToMany(targetEntity="Poll", mappedBy="author")
     */
    private $polls_created;

   /**
    * User constructor.
    * @param string $username
    */
   public function __construct(string $username) {
      $this->username = $username;
      $this->votes = new ArrayCollection();
      $this->polls_created = new ArrayCollection();
   }
 
   public function getId() {
    return $this->id;
    }

   public function getRoles() {
      return [$this->roles];
   }
 
   public function getPassword() : string {
      return $this->password;
   }
 
   public function getSalt() {
      // TODO: Implement getSalt() method.
   }
 
   public function getUsername() {
      return $this->username;
   }
 
   public function eraseCredentials() {
      // TODO: Implement eraseCredentials() method.
   }

    public function setRoles(string $role) {
        $this->roles = $role;
    }

   public function setPassword(string $password) {
      $this->password = $password;
   }
 
}