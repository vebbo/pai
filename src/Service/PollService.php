<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\Poll;
use App\Entity\PollVote;
use App\Entity\PollOption;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PollService
{
    private $entityManager;
    private $pollBuilder;

    public function __construct(EntityManagerInterface $entityManager, PollBuilder $pollBuilder)
    {
        $this->entityManager = $entityManager;
        $this->pollBuilder = $pollBuilder;
    }

    public function addUserVote($user, $pollId, $optionId)
    {
        $existingVote = $this->entityManager->getRepository(PollVote::class)->findUserVote($user->getId(), $pollId);
        if ($existingVote)
            throw new BadRequestHttpException('Głos już oddany');
        
        $poll = $this->entityManager->getRepository(Poll::class)->find($pollId);
        if ($poll->getIsRunning() == false)
            throw new BadRequestHttpException('Głosowanie zakończone');

        $option = $this->entityManager->getRepository(PollOption::class)->find($optionId);

        $vote = new PollVote();
        $vote->setVoter($user);
        $vote->setOption($option);
        $vote->setPoll($poll);

        $this->entityManager->persist($vote);
        $this->entityManager->flush();
    }

    public function addPoll($user, $model)
    {
        $poll = $this->pollBuilder->buildPoll($user, $model);
        $this->entityManager->persist($poll);

        foreach ($model['options'] as $option)
        {
            $newOption = $this->pollBuilder->buildPollOption($poll, $option);
            $this->entityManager->persist($newOption);
        }    

        $this->entityManager->flush();
    }

    public function closePoll($pollId)
    {
        $poll = $this->entityManager->getRepository(Poll::class)->find($pollId);
        if (!$poll)
            throw new NotFoundHttpException();

        if ($poll->getIsRunning() == false)
            throw new BadRequestHttpException('Głosowanie już zakończone');

        $poll->setIsRunning(false);

        $this->entityManager->merge($poll);
        $this->entityManager->flush();
    }
}