<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\Poll;
use App\Entity\PollOption;

class PollBuilder
{
    public function buildPoll($user, $model)
    {
        $poll = new Poll();
        $poll->setName($model['name'])
            ->setText($model['text'])
            ->setAuthor($user)
            ->setIsRunning(true);

        return $poll;
    }

    public function buildPollOption($poll, $text)
    {
        $option = new PollOption();
        $option->setText($text)
            ->setPoll($poll);

        return $option;
    }
}