<?php 
namespace App\Controller; 

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request; 
use Symfony\Component\HttpFoundation\Response; 
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils; 
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; 
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController 
{
   /**
    * @Route("/login", name="login")
    */
   public function login(Request $request, AuthenticationUtils $authenticationUtils) : Response 
   {
      $errors = $authenticationUtils->getLastAuthenticationError();
      $lastUsername = $authenticationUtils->getLastUsername();

      return $this->render('User/Login.html.twig', [
         'errors' => $errors,
         'username' => $lastUsername
      ]);
   }

   /**
    * @Route("/register", name="register")
    */
    public function register(Request $request, AuthenticationUtils $authenticationUtils) : Response 
    {
       return $this->render('User/Register.html.twig', []);
    }

   /**
     * @Route("/registerUser", name="registerUser", methods={"POST"})
     */
    public function registerUser(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
         $user = new User($request->request->get('username'));
         $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));
         $user->setRoles('ROLE_USER');

         $entityManager = $this->getDoctrine()->getManager();
         $entityManager->persist($user);
         $entityManager->flush();

         return $this->redirectToRoute('index');
    }

   /**
    * @Route("/logout", name="logout")
    */
   public function logout() : Response 
   {
   }
}