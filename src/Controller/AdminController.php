<?php
namespace App\Controller;

use App\Service\PollService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\User\UserInterface;

class AdminController extends AbstractController
{
    private $pollService;

    public function __construct(PollService $pollService)
    {
        $this->pollService = $pollService;
    }

    /**
    * @Route("/admin/addpoll")
    */
    public function addPoll()
    {
        return $this->render('Polls/Add.html.twig', []);
    }

    /**
    * @Route("/admin/savepoll", methods={"POST"})
    */
    public function savePoll(Request $request, UserInterface $user)
    {
        $model = [
            'name' => $request->request->get('name'),
            'text' => $request->request->get('text'),
            'options' => $request->request->get('options')
        ];

        $this->pollService->addPoll($user, $model);

        return new Response("");
    }
}
