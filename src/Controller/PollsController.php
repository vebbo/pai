<?php
namespace App\Controller;

use App\Entity\Poll;
use App\Service\PollService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request; 
use Symfony\Component\HttpFoundation\Response; 
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\User\UserInterface;

class PollsController extends AbstractController
{
    private $pollRepository;
    private $pollService;

    public function __construct(EntityManagerInterface $entityManager, PollService $pollService)
    {
        $this->pollRepository = $entityManager->getRepository(Poll::class);
        $this->pollService = $pollService;
    }

    /**
    * @Route("/polls/open")
    */
    public function open()
    {
        return $this->render('Polls/List.html.twig', [
            'heading' => 'Aktywne ankiety',
            'open' => true,
            'polls' => $this->pollRepository->findActive(true)
        ]);
    }

    /**
    * @Route("/polls/closed")
    */
    public function closed()
    {
        return $this->render('Polls/List.html.twig', [
            'heading' => 'Zamknięte ankiety',
            'open' => false,
            'polls' => $this->pollRepository->findActive(false)
        ]);
    }

    /**
    * @Route("/polls/vote/{id}")
    */
    public function vote($id)
    {
        return $this->render('Polls/Vote.html.twig', [
            'poll' => $this->pollRepository->findOneById($id)
        ]);
    }

    /**
     * @Route("/polls/vote", methods={"POST"})
     */
    public function sendVote(Request $request, UserInterface $user)
    {
        $pollId = $request->request->get('pollId');
        $optionId = $request->request->get('optionId');

        $this->pollService->addUserVote($user, $pollId, $optionId);

        return new Response("");
    }

     /**
    * @Route("/polls/results/{id}")
    */
    public function results($id)
    {
        return $this->render('Polls/Results.html.twig', [
            'poll' => $this->pollRepository->findOneById($id)
        ]);
    }

     /**
     * @Route("/admin/closepoll/{id}", methods={"POST"})
     */
    public function closePoll($id)
    {
        $this->pollService->closePoll($id);

        return new Response("");
    }
}