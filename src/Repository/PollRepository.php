<?php

namespace App\Repository;

use App\Entity\Poll;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Poll|null find($id, $lockMode = null, $lockVersion = null)
 * @method Poll|null findOneBy(array $criteria, array $orderBy = null)
 * @method Poll[]    findAll()
 * @method Poll[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PollRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Poll::class);
    }

    /**
    * @return Poll[] Returns an array of Poll objects
    */
    public function findActive($available)
    {
        return $this->createQueryBuilder('p')
            ->leftJoin('p.votes', 'v')
            ->andWhere('p.is_running = :is_running')
            ->setParameter('is_running', $available ? 1 : 0)
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findOneById($id): ?Poll
    {
        return $this->createQueryBuilder('p')
            ->leftJoin('p.votes', 'v')
            ->andWhere('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
