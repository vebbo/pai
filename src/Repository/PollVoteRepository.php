<?php

namespace App\Repository;

use App\Entity\PollVote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PollVote|null find($id, $lockMode = null, $lockVersion = null)
 * @method PollVote|null findOneBy(array $criteria, array $orderBy = null)
 * @method PollVote[]    findAll()
 * @method PollVote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PollVoteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PollVote::class);
    }

    public function findUserVote($userId, $pollId)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.voter = :voter')
            ->andWhere('p.poll = :poll')
            ->setParameter(':voter', $userId)
            ->setParameter(':poll', $pollId)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
