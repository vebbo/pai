<?php
namespace App\DataFixtures;
 
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
 
class UserFixtures extends Fixture {
 
   /**
    * @var UserPasswordEncoderInterface
    */
   private $encoder;
 
   /**
    * @var EntityManager
    */
   private $entityManager;
 
   /**
    * UserFixtures constructor.
    * @param UserPasswordEncoderInterface $encoder Password encoder instance
    */
   public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $entityManager) {
      $this->encoder = $encoder;
      $this->entityManager = $entityManager;
   }
 
   /**
    * @param ObjectManager $manager Object manager instance
    *
    * @return void
    */
   public function load(ObjectManager $manager) : void {
      $u1 = new User('admin');
      $u1->setRoles("ROLE_ADMIN");
      $u1->setPassword($this->encoder->encodePassword($u1, 'admin'));
 
      $this->entityManager->persist($u1);

      $u2 = new User('user');
      $u2->setRoles("ROLE_USER");
      $u2->setPassword($this->encoder->encodePassword($u2, 'user'));
 
      $this->entityManager->persist($u2);

      $this->entityManager->flush();
   }
}